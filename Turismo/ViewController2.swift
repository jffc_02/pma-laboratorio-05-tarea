
import UIKit

class ViewController2: UIViewController,UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet var Tabladestinos: UITableView!
    
    var destinos = [Destino]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Tabladestinos.dataSource = self
        Tabladestinos.delegate = self
        print(destinos)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return destinos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celdaDestino = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "celdaDestino")
        celdaDestino.textLabel?.text = destinos[indexPath.row].titulo
        return celdaDestino
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recursoSeleccionado = indexPath.row
        
        self.performSegue(withIdentifier: "vistaTresSegue", sender: recursoSeleccionado)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "vistaTresSegue"{
            let idrecursoRecibido = sender as! Int
            let pantalla3:ViewController3 = segue.destination as! ViewController3
            pantalla3.imagenNombre = destinos[idrecursoRecibido].imagen
            pantalla3.destinoNombre = destinos[idrecursoRecibido].titulo
        }
    }

}
