import UIKit

class ViewController3: UIViewController {
    
    @IBOutlet weak var Imagen: UIImageView!
    @IBOutlet weak var DestinoTexto: UILabel!
    
    var imagenNombre: String = ""
    var destinoNombre: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Imagen.image = UIImage(named: imagenNombre)
        DestinoTexto.text = destinoNombre
        Imagen.layer.borderWidth = 2
        Imagen.layer.borderColor = UIColor.red.cgColor
        Imagen.layer.cornerRadius = 25
        Imagen.contentMode = .scaleAspectFill
    }
}
