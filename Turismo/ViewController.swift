
import UIKit


class Pais {
    let nombre:String
    var destinos:[Destino]
    
    init (_ nombre:String, _ destinos: [Destino]){
        self.nombre = nombre
        self.destinos = destinos
    }
}

class Destino {
    let titulo:String
    let imagen:String
    
    init (_ titulo:String,_ imagen:String){
        self.titulo = titulo
        self.imagen = imagen
    }
}

var destino1 = Destino("Machu Picchu","machupichu")
var destino2 = Destino("Cañon del Colca","colca")
var destino3 = Destino("Torre Eiffel","torreEifel")
var destino4 = Destino("Costa azul","costaAzul")
var destino5 = Destino("Castillo de Alcazar","castilloAlcazar")
var destino6 = Destino("Palacio real","palacioReal")

var pais1 = Pais("Peru",[destino1,destino2])
var pais2 = Pais("Francia",[destino3,destino4])
var pais3 = Pais("España",[destino5,destino6])

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var TablaPaises: UITableView!
    
    let paises = [pais1,pais2,pais3]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TablaPaises.dataSource = self
        TablaPaises.delegate = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return paises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celda = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "celda")
        celda.textLabel?.text = paises[indexPath.row].nombre
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recursoSeleccionado = indexPath.row
        self.performSegue(withIdentifier: "vistaDosSegue", sender: recursoSeleccionado)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "vistaDosSegue"{
            let idrecursoRecibido = sender as! Int
            let pantalla2:ViewController2 = segue.destination as! ViewController2
            print(idrecursoRecibido)
            print(paises[idrecursoRecibido].nombre)
            pantalla2.destinos = paises[idrecursoRecibido].destinos
        }
    }
}





